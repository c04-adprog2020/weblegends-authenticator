const express = require("express");
const app = express();
const port = process.env.PORT || 4000;
const CONNECTION_URL =
  "mongodb://yogahmad:abcdef123@cluster0-shard-00-00-ujmqy.mongodb.net:27017,cluster0-shard-00-01-ujmqy.mongodb.net:27017,cluster0-shard-00-02-ujmqy.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";

const mongoose = require("mongoose");

mongoose.connect(
  CONNECTION_URL,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log("Database Started");
  }
);

const db = mongoose.connection;
db.on("error", err => console.error(err));
db.once("open", () => console.log("Connected to database"));

app.use(express.json());

const accountRoutes = require("./routes/account");
app.use("/account", accountRoutes);
const apiRoutes = require("./routes/api");
app.use("/api", apiRoutes);

app.listen(port, () => {console.log('Express server listening on port', port)});

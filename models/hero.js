const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const HeroSchema = new Schema({
  gameId: {
    type: String,
    unique: true,
    required: true,
  },
  data: {
    type: String,
  },
});

module.exports = mongoose.model("Hero", HeroSchema);

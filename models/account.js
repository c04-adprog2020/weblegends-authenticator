const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AccountSchema = new Schema({
  username: {
    type: String,
    maxlength: 20,
    minlength: 4,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  gameId: {
    type: String,
    required: true
  },
  score: {
    type: Number,
    default: 0
  }
});

module.exports = mongoose.model("Account", AccountSchema);

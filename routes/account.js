const express = require("express");
const Account = require("../models/account");
const router = express.Router();
const bcrypt = require("bcrypt");
const Axios = require("axios");

router.use("/", (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//REGISTER ACCOUNT, REQUIRED USERNAME, EMAIL, PASSWORD
router.post("/register/", async (req, res) => {
  const account = await new Account({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
    gameId: "none",
  });
  try {
    const newAccount = await account.save();
    res.status(201).json({ message: "OK" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

router.post("/update/", async (req, res) => {
  try {
    const account = await Account.findOne({ username: req.body.username });
    account.gameId = req.body.gameId;
    await account.save();
    res.status(200).json(account);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//LOGIN ACCOUNT, REQUIRED USERNAME, PASSWORD
router.post("/login/", async (req, res) => {
  try {
    const account = await Account.findOne({ username: req.body.username });
    if (!account) res.status(401).json({ message: "Username does not exist!" });
    if (!bcrypt.compareSync(req.body.password, account.password))
      res.status(401).json({ message: "Wrong password!" });
    res.status(201).json({ token: account._id });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//AUTHENTICATE TOKEN, REQUIRED ACCOUNT ID
router.post("/authenticate/", async (req, res) => {
  try {
    const account = await Account.findOne({ _id: req.body.token });
    if (!account) res.status(401).json({ message: "Invalid token" });
    res.status(201).json({
      token: account._id,
      gameId: account.gameId,
      username: account.username,
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

//CREATE CHARACTER
router.post("/create/", async (req, res) => {
  const account = await Account.findById(req.body.token);
  if (account.gameId !== "none") {
    await Axios.delete("https://c04-database-manager-service.herokuapp.com/player/" + account.gameId);
  }
  console.log(account.gameId);
  
  try {
    const result = await Axios.post(
      "https://c04-database-manager-service.herokuapp.com/player",
      {
        name: account.username,
        heroClass: req.body.char,
        score: 0,
      }
    );
    console.log(result.data);
    account.gameId = result.data["id"];
    account.score = 0;
    await account.save();
    res.status(200).json({ token: account._id, score: account.score });
  } catch (err) {
    console.log(err);

    res.status(400).json({ message: err.message });
  }
});

//GET SCORE
router.post("/getscore/", async (req, res) => {
  const account = await Account.findById(req.body.token);
  res.status(200).json({ score: account.score, gameId: account.gameId });
});

router.get("/leaderboard/", async (req, res) => {
  try {
    const result = await Axios.get(
      "https://c04-database-manager-service.herokuapp.com/player/top-score/10"
    );
    res.status(200).json(result.data);
  } catch (error) {
    res.status(400).json({ message: err.message });
  }
});

module.exports = router;

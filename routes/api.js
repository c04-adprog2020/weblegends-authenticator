const Axios = require("axios");
const express = require("express");
const router = express.Router();
const Account = require("../models/account");
const Hero = require("../models/hero");
const bcrypt = require("bcrypt");

router.use("/", (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

router.get("/leaderboard/", async (req, res) => {
  try {
    const result = await Axios.get(
      "https://c04-database-manager-service.herokuapp.com/player/top-score/10"
    );
    res.status(200).json(result.data);
  } catch (error) {
    res.status(400).json({ message: err.message });
  }
});

router.post("/login/", async (req, res) => {
  try {
    const account = await Account.findOne({ username: req.body.username });
    if (!account) res.status(400).json({ message: "Username does not exist!" });
    if (!bcrypt.compareSync(req.body.password, account.password))
      res.status(400).json({ message: "Wrong password!" });
    account.password = req.body.password;
    res.status(200).json({ account: account });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/register/", async (req, res) => {
  const account = new Account({
    username: req.body.username,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 10),
    gameId: "none",
  });
  try {
    await account.save();
    res.status(200).json({ account: account });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

//CREATE CHARACTER
router.post("/create/", async (req, res) => {
  try {
    const account = await Account.findOne({ username: req.body.username });
    if (account.gameId !== "none") {
      await Axios.delete(
        "https://c04-database-manager-service.herokuapp.com/player/" +
          account.gameId
      );
    }

    let result = await Axios.post(
      "https://c04-database-manager-service.herokuapp.com/player",
      {
        name: account.username,
        heroClass: req.body.heroClass,
        score: 0,
      }
    );
    account.gameId = result.data["id"];
    account.score = 0;
    await account.save();

    result = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/player-api/produce-hero",
      {
        name: account.username,
        className: req.body.heroClass,
      }
    );

    res.status(200).json({ id: account.gameId, data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/spawn-enemy/", async (req, res) => {
  try {
    let result = await Axios.get(
      "http://weblegends-gameplay.herokuapp.com/battle-api/spawn-enemy?level=" +
        req.body.level
    );

    res.status(200).json({ data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/attack/", async (req, res) => {
  try {
    let result = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/battle-api/player-attack",
      {
        player: req.body.player,
        enemy: req.body.enemy,
      }
    );
    res.status(200).json({ data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/skill/", async (req, res) => {
  try {
    let result = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/battle-api/player-skill",
      {
        player: req.body.player,
        enemy: req.body.enemy,
      }
    );

    res.status(200).json({ data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/enemy-attack/", async (req, res) => {
  try {
    let result = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/battle-api/enemy-attack",
      {
        player: req.body.player,
        enemy: req.body.enemy,
      }
    );

    res.status(200).json({ data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/upgrade/", async (req, res) => {
  try {
    let result = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/player-api/upgrade-hero",
      req.body
    );

    let response = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/player-api/enhance-weapon",
      result.data
    );

    res.status(200).json({ data: response.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/drop-item/", async (req, res) => {
  try {
    const result = await Axios.get(
      "http://weblegends-gameplay.herokuapp.com/item-api/drop-item"
    );
    res.status(200).json({ data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/use-item/", async (req, res) => {
  try {
    console.log(req.body);
    const result = await Axios.post(
      "http://weblegends-gameplay.herokuapp.com/item-api/use-item",
      req.body
    );
    res.status(200).json({ data: result.data });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/update-score/", async (req, res) => {
  try {
    const account = await Account.findOne({ username: req.body.username });
    await Axios.get(
      `https://c04-database-manager-service.herokuapp.com/player/update/${account.gameId}/score/${req.body.newScore}`
    );
    account.score = req.body.newScore;
    await account.save();

    res.status(200).json({ message: "OK" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/save-hero/", async (req, res) => {
  try {
    let hero = await Hero.findOne({ gameId: req.body.gameId });
    if (hero === null) {
      hero = new Hero({
        gameId: req.body.gameId,
      });
    }
    hero.data = JSON.stringify(req.body);
    await hero.save();
    res.status(200).json({ message: "OK" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

router.post("/load-hero/", async (req, res) => {
  try {
    let hero = await Hero.findOne({ gameId: req.body.gameId });
    if (hero === null) throw "no hero";
    let data = JSON.parse(hero.data);
    const result = await Axios.get(
      "https://c04-database-manager-service.herokuapp.com/player/" +
        req.body.gameId
    );
    data["type"] = result["heroClass"];
    res.status(200).json({ data: JSON.parse(hero.data) });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: err.message });
  }
});

module.exports = router;
